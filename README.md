Definition of the base worker images for CI.

These images are used both as Slaves in Jenkins and Workers on GitLab-CI.

The same definition was used to build both SLC6 and CC7 images, now only CC7 active (SLC6 EOL November 2020). It is expected that customized
workers images are derived from these images, and will be updated automatically when the base
images are.

Contribute
-----------
By default, this project doesn't allow new branches to be pushed. To contribute,
fork it and submit a Pull Request.

By enforcing the use of forks, we ensure that images generated when doing development will be stored in the user's fork
registry space instead of in the main repository, which will therefore only contain production images.

To block pushing to any branch, we use the `Protected branches` feature of GitLab with a `*` wildcard.
